---
title: 龙芯杯MIPS-GCC交叉编译环境配置
categories: arch
description: 
swiper_index: 1
abbrlink: 2e7856a2
cover: https://qiniu.miyamura.top/images/wlop/wl9.jpg
---

<!--<font color=#b0b0b0>**首次发布日期：2023/3/11*</font>-->
NSCSCC（龙芯杯）提供的龙芯编译环境。

---

## 下载安装

---

下载龙芯提供的编译环境配置压缩包：
[gcc-4.3-ls232.tar.gz](http://os.cs.tsinghua.edu.cn/oscourse/project/LoongsonCsprj2018)
终端下进入gcc-4.3-ls232.tar.gz所在目录，将环境加压至根目录。

```shell
sudo tar -zxvf gcc-4.3-ls232.tar.gz -C /

```

向.bashrc文件添加路径。

```shell
echo "export PATH=/opt/gcc-4.3-ls232/bin:$PATH" >> ~/.bashrc

```

对于64位系统，还要安装32位环境支持。

```shell
sudo apt-get install lsb-core

```

某 <font color=#b0b0b0>~~CPU设计实战~~</font> 书上还要求安装lib32ncurses-dev，装的时候报Unable to locate package，去[查了一下](https://packages.ubuntu.com/)发现Ubuntu下根本没这个包，最相近的叫lib32ncurses5-dev，装了一下，好像没什么用。
如果可以输入```mipsel-linux-gcc -v```命令查看版本号则安装成功。

## WSL环境下编译测试程序的问题

---

如果用WSL2作为Linux环境编译测试程序（测试程序放在Windows目录下，在Linux终端中通过/mnt目录访问Windows磁盘），会产生如下报错：

```
Value too large for defined data type

```

原因是目标文件inode号过大，超过32位系统的识别范围，可以ls -li验证一下。
网上给了很多解决方案，试了一下没有奏效的。最后果然最简单的办法最有效，把文件拷贝到Linux目录下，inode号就变得正常了，make好了再拷回Windows目录。

---

折腾半天就是为了gs132跑出的trace文件和ram_ip初始化的inst_ram.coe文件。以后有时间可能会整理一下发个仓库，避免重复劳动。

## 引用

---

[unable-to-locate-package-error-ubuntu](https://itsfoss.com/unable-to-locate-package-error-ubuntu/)
[packages.ubuntu.com](https://packages.ubuntu.com/)
[清华uCore实验-移植MIPS软核](https://oscourse-tsinghua.gitbook.io/loongsoncsprj2020-manual/)
[mipsel-linux-gcc工具链搭建](https://blog.csdn.net/weixin_45073610/article/details/118608443)
[GCC编译“Value too large for defined data type”错误解决办法](https://blog.csdn.net/gladyoucame/article/details/84020819?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522167846875416800188598134%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=167846875416800188598134&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~blog~first_rank_ecpm_v1~rank_v31_ecpm-1-84020819-null-null.article_score_rank_blog&utm_term=GCC%E7%BC%96%E8%AF%91&spm=1018.2226.3001.4450)
[解决Value too large for defined data type问题](https://blog.csdn.net/qq_43189737/article/details/83000619?spm=1001.2101.3001.6650.1&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-1-83000619-blog-84020819.pc_relevant_multi_platform_whitelistv3&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-1-83000619-blog-84020819.pc_relevant_multi_platform_whitelistv3&utm_relevant_index=2)
[Linux下出现Value too large for defined data type的报错](https://blog.csdn.net/qq_37273284/article/details/118052940?spm=1001.2101.3001.6661.1&utm_medium=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1-118052940-blog-83000619.pc_relevant_3mothn_strategy_recovery&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1-118052940-blog-83000619.pc_relevant_3mothn_strategy_recovery&utm_relevant_index=1)
[inode爆炸问题解决](https://www.haoshuang.site/2020/06/08/os-project/)
[gcc编译参数概述](https://www.cnblogs.com/dylancao/p/9681663.html)
