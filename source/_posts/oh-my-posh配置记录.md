title: oh-my-posh配置记录
description: 使用 oh-my-posh 美化 windows terminal
abbrlink: ohmyposh
cover: https://qiniu.miyamura.top/images/wlop/wl11.jpg
---

## 配置流程

---

[https://ohmyposh.dev/docs/](https://ohmyposh.dev/docs/)
Installation → Windows → manual，复制指令进终端：

```bash
Set-ExecutionPolicy Bypass -Scope Process -Force; Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://ohmyposh.dev/install.ps1'))

```

安装oh-my-posh.exe及下载主题。
装好后会自动配置环境变量```POSH_THEMES_PATH```，是各种主题的json文件所在路径，以后可以通过`$env:POSH_THEMES_PATH`调用。
然后开始配置主题，首先查看powershell配置文件路径：

```bash
# powershell profile script
# 这个文件会在每次启动终端时调用
$PROFILE

```

得到一个后缀为.ps1的文件路径。我们希望每次启动powershell时，都可以加载oh-my-posh对页面进行美化，编辑该文件：

```bash
# 如果提示路径不存在，手动创建一下即可
notepad $PROFILE

```

输入如下指令：

```bash
# 第一条不加应该也可以，我是删文件不小心把PSReadLine也删了，后来还原的
Import-Module PSReadLine
# $env:POSH_THEMES_PATH\<theme_name.json>，去上文提到的环境变量里看看，这里我选的是powerline
oh-my-posh init pwsh --config $env:POSH_THEMES_PATH\powerline.omp.json | Invoke-Expression

```

保存后重启powershell，已经初步看到美化效果，但icon部分还是乱码。如果之前没有配置```Set-ExecutionPolicy```，会显示配置无法加载。

## 字体配置

---

下载[Nerd Fonts](https://www.nerdfonts.com/)，该字体为oh-my-posh所支持。任选一样式下载，解压后全选安装在系统字体库，路径为C:\Windows\Fonts。然后修改Windows Terminal的配置（不是powershell的配置！），在终端窗口设置里选中powershell → 外观 → 字体，或直接修改外观配置文件，路径为%LOCALAPPDATA%\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json。
[Windows终端中的外观配置文件设置](https://docs.microsoft.com/zh-cn/windows/terminal/customize-settings/profile-appearance)
这个文件中我们主要关注如下结构：

```xml
{	
	...
	
	"profiles":
	{
		"defaults":
		{
			"font":
			{
				// 字体名称去字体文件夹里找，或者在官网
				// 如"face": "CodeNewRoman Nerd Font"
				"face": "<FontName>",
			},
		},
		"list":
		[
			...
		]
	}
	
	...
}
```

找到`"defaults": {}`，手动添加font字段进去，配置后保存，这时终端能正常加载主题和字体了。
至此，oh-my-posh for powershell配置完成。
还可以设置一下终端背景，同样在defaults下加入以下字段：

```bash
"backgroundImage": "<imgpath>",
"backgroundImageAlignment": "bottomRight",
"backgroundImageOpacity": 0.3,
"backgroundImageStretchMode": "uniformToFill"

```

最终的完整终端界面：![在这里插入图片描述](https://img-blog.csdnimg.cn/73fa60156ec241fdb06233a5957fc8dd.png)

---

现在发现，按Microsoft指导安装才是道门正派
[https://learn.microsoft.com/zh-cn/windows/terminal/tutorials/custom-prompt-setup](https://learn.microsoft.com/zh-cn/windows/terminal/tutorials/custom-prompt-setup)

## oh my posh for Linux

---

参考[Oh My Posh 文档中的 Linux 安装指南](https://ohmyposh.dev/docs/installation/linux)进行安装。因为我在一台ARM设备上安装，而Homebrew暂不支持ARM64，故采用手动安装。

```bash
# 注意把posh-linux-arm64换成你设备的ISA
sudo wget https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/posh-linux-arm64 -O /usr/local/bin/oh-my-posh
sudo chmod +x /usr/local/bin/oh-my-posh

```

下面是重新在 Linux 下安装了一遍主题库，这一步可以省略

```bash
mkdir ~/.poshthemes
wget https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/themes.zip -O ~/.poshthemes/themes.zip
unzip ~/.poshthemes/themes.zip -d ~/.poshthemes
chmod u+rw ~/.poshthemes/*.omp.*
rm ~/.poshthemes/themes.zip

```

配置.bashrc文件。在.profile里配置也是可行的。

[linux关于bashrc与profile的区别](https://cloud.tencent.com/developer/article/1174426)

```bash
eval "$(oh-my-posh init bash --config ~/.poshthemes/powerline.omp.json)"

```

如果没有重装主题库，则路径改为/mnt/<Windows下的主题路径>/powerline.omp.json。字体设置同powershell。重启Bash，成功看到美化效果。

## 参考

---

[https://blog.csdn.net/lz6363/article/details/118876966](https://blog.csdn.net/lz6363/article/details/118876966)
[https://blog.csdn.net/weixin_42818618/article/details/118104218](https://blog.csdn.net/weixin_42818618/article/details/118104218)
[https://blog.csdn.net/ba_wang_mao/article/details/121220935](https://blog.csdn.net/ba_wang_mao/article/details/121220935)
[https://blog.csdn.net/qq_43108090/article/details/121687415](https://blog.csdn.net/qq_43108090/article/details/121687415)
[https://blog.csdn.net/weixin_30920513/article/details/97960798](https://blog.csdn.net/weixin_30920513/article/details/97960798)
[https://blog.csdn.net/weixin_43924623/article/details/107947006](https://blog.csdn.net/weixin_43924623/article/details/107947006)

---

*黑夜来了，我驶进银河的港湾。*
