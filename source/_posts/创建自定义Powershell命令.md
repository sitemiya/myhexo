---
title: PowerShell创建自定义命令
categories:
- Terminal
abbrlink: 2e9b24be
swiper_index: 2
description: 将 .ps1 脚本封装为 PowerShell 命令
cover: https://qiniu.miyamura.top/images/wlop/wl4.jpg
---

<!--
<meta name="referrer" content="no-referrer" />
-->

## 功能

---

生成如下文件结构，添加文件夹至VSCode工作区，并打开生成的markdown文件

```
$Disk:.
└─$name─$name.md
    └─imgpasted
	
```

提供如下可选参数

```
-name     : markdown文件/文件夹名
-savepath : 保存路径（相对路径）

```

## 脚本

---

新建.ps1文件，输入如下内容

```powershell
# 指定命令行参数，并提供默认值
#
# name     : 默认为时间戳
# savepath : 默认为统一存放markdown的文件夹

param(
	[Parameter()]
	[String]$name = "$(Get-Date -Format 'yyyy-mm-dd-hh-mm-ss')",	
	[String]$savepath = "$([Environment]::GetFolderPath("Desktop"))\mdfiles\"
)

$exist = $False

# 获取$savepath下所有内容的名称并沿管道传送
# 遍历检查是否重名，打印错误信息并将$exist设为真
# 注意大括号不能分行写

Get-ChildItem -Path $savepath -Name | Foreach-Object -Process {
	if ([String]$_ -eq $name)
	{
		Write-Host "[$(Get-Date)]makemd: Error: $savepath$name already existed.`n" -ForegroundColor red
		$exist = $True
	}
}

# 未发现重命名则创建文件、文件夹
# code -a <dir> : 将文件夹添加至VSCode工作区
# code <file>   : 打开文件至编辑区
# 打印提示信息

if ($exist -eq $False)
{
	mkdir "$savepath$name", "$savepath$name\imgpasted"
	new-item "$savepath$name\$name.md" -type file
	code -a "$savepath$name"
	code "$savepath$name\$name.md"
	Write-Host "[$(Get-Date)]makemd: $savepath$name created successfully.`n" -ForegroundColor green
}

```

保存后打开终端，可以脚本方式运行。

## 创建PowerShell命令

假定命令名为：`Makemd`，为保持PowerShell命令风格，首字母大写。

---

查看PowerShell命令的搜索路径

```powershell
echo "$env:PSModulePath"

```

将脚本放在位于Program Files的路径下，我这里为`C:\Program Files\WindowsPowerShell\Modules`。首先对.ps1文件做如下修改

```powershell
function Makemd
{
	# 这里放脚本内容
}

```

在上一步的路径（Modules）下新建文件夹Makemd，在其内放置修改后的脚本，并重命名为Makemd.psm1。
 - 文件夹/文件对应关系：包含新模块的文件夹内至少含一个同名文件，如这里的Makemd/Makemd.psm1
 - 文件/函数对应关系：函数可与文件不同名，一个.psm1文件可包含多个函数

---

为脚本代码套壳有什么意义呢？不妨不修改为函数，直接粘贴.ps1内容至.psm1，查询模块

```powershell
# Get-Command会执行Import-Module Makemd，查询＋导入
Get-Command -Module Makemd # 文件夹

```

将打印成功提示信息，可知脚本按默认参数执行了一次。然而键入Makemd（函数名），发现命令无法找到

```
Makemd: 无法将“Makemd”项识别为 cmdlet、函数、脚本文件或可运行程序的名称。

```

因此需要修改为函数，执行`Remove-Module Makemd`，然后重新导入模块，打印如下信息

```
CommandType Name         Version Source
----------- ----         ------- ------
Function    Get-OldFiles 0.0     Makemd

```

可以看到函数成功导入，这时就可以在终端里使用命令了。

> 在某个PowerShell窗口找不到命令时，关闭并新建一个试试
每次更新.psm1文件后需移除模块并重新导入

正常情况下，使用该命令的时候是无需导入该模块的，相比脚本也不需要写路径，十分方便。


## 附录

---

### PowerShell 常用

System.ConsoleColor

```
Black, DarkBlue, DarkGreen, DarkCyan, DarkRed, DarkMagenta, DarkYellow, Gray, DarkGray, Blue, Green, Cyan, Red, Magenta, Yellow, White

```

编辑配置文件

```powershell
notepad $PROFILE

```

## 参考

---

[VSCode命令行常用快捷操作](https://blog.csdn.net/bingjianIT/article/details/84189606)
[PowerShell获取时间格式](https://www.jb51.net/article/53282.htm)
[在 Windows PowerShell 中获取命令行参数](https://www.delftstack.com/zh/howto/powershell/command-line-arguments-in-powershell/)
[PowerShell脚本输出颜色文字](https://www.azimiao.com/6512.html)
[Powershell如何查询目录所有文件和文件夹的名称](https://blog.csdn.net/weixin_43838488/article/details/119327713)
[在 PowerShell 中比较两个字符串对象的内容](https://www.delftstack.com/zh/howto/powershell/compare-the-contents-of-two-string-objects-in-powershell/)
[自定义供日常使用的PowerShell脚本模块](https://zhuanlan.zhihu.com/p/429002760)
[用windows Powershell 创建新文件夹或文件](https://blog.csdn.net/Stinky_kiss/article/details/38519363)
[如何在Windows PowerShell中获取当前的用户名](https://blog.csdn.net/CHCH998/article/details/107726143)
[获取Windows某路径的环境变量](https://www.codenong.com/2000638/)
[PowerShell列出某个文件夹内的所有文件和文件夹](https://www.yiibai.com/code/detail/15310)
