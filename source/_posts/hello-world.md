top: true
sticky: 1
abbrlink: 4a17b156
hide: true
published: false
title: Hello World
tags:
  - Hexo
categories:
  - Hello
aside: false
---

Welcome to [Hexo](https://hexo.io/)! This is your very first post. Check [documentation](https://hexo.io/docs/) for more info. If you get any problems when using Hexo, you can find the answer in [troubleshooting](https://hexo.io/docs/troubleshooting.html) or you can ask me on [GitHub](https://github.com/hexojs/hexo/issues).

## Quick Start

### Create a new post

``` bash
$ hexo new "My New Post"
```

More info: [Writing](https://hexo.io/docs/writing.html)

### Run server

``` bash
$ hexo server
```

More info: [Server](https://hexo.io/docs/server.html)

### Generate static files

``` bash
$ hexo generate
```

More info: [Generating](https://hexo.io/docs/generating.html)

### Deploy to remote sites

``` bash
$ hexo deploy
```

More info: [Deployment](https://hexo.io/docs/one-command-deployment.html)


### pic_bed test

[-_-]: ![android-chrome-192x192.png](https://qiniu.miyamura.top/images/2023/12/21/61730fffecdb2564924c7c8dab002b16.png?imageMogr2/thumbnail/800x/strip/quality/80)

{% tabs test4 %}
<!-- tab 第一个Tab -->
**tab名字为第一个Tab**
<!-- endtab -->

<!-- tab @fab fa-apple-pay -->
**只有图标 没有Tab名字**
<!-- endtab -->

<!-- tab 炸弹@fas fa-bomb -->
**名字+icon**
<!-- endtab -->
{% endtabs %}

{% note simple %}
默认 提示块标签
{% endnote %}

{% note default simple %}
default 提示块标签
{% endnote %}

{% note primary simple %}
primary 提示块标签
{% endnote %}

{% note success simple %}
success 提示块标签
{% endnote %}

{% note info simple %}
info 提示块标签
{% endnote %}

{% note warning simple %}
warning 提示块标签
{% endnote %}

{% note danger simple %}
danger 提示块标签
{% endnote %}

{% score %}
X:1
T:alternate heads
M:C
L:1/8
U:n=!style=normal!
K:C treble style=rhythm
"Am" BBBB B2 B>B | "Dm" B2 B/B/B "C" B4 |"Am" B2 nGnB B2 nGnA | "Dm" nDB/B/ nDB/B/ "C" nCB/B/ nCB/B/ |B8| B0 B0 B0 B0 |]
%%text This translates to:
[M:C][K:style=normal]
[A,EAce][A,EAce][A,EAce][A,EAce] [A,EAce]2 [A,EAce]>[A,EAce] |[DAdf]2 [DAdf]/[DAdf]/[DAdf] [CEGce]4 |[A,EAce]2 GA [A,EAce] GA |D[DAdf]/[DAdf]/ D[DAdf]/[DAdf]/ C [CEGce]/[CEGce]/ C[CEGce]/[CEGce]/ |[CEGce]8 | [CEGce]2 [CEGce]2 [CEGce]2 [CEGce]2 |]
GAB2 !style=harmonic![gb]4|GAB2 [K: style=harmonic]gbgb|
[K: style=x]
C/A,/ C/C/E C/zz2|
w:Rock-y did-nt like that
{% endscore %}

{% timeline 2022,pink %}
<!-- timeline 01-02 -->
这是测试页面
<!-- endtimeline -->
{% endtimeline %}

{% link 糖果, https://blog.miyamura.top/posts/14ff645c.html/#配置流程, /img/android-chrome-192x192.png %}

{% link 糖果屋教程贴, https://akilar.top/posts/615e2dec/, /img/android-chrome-192x192.png %}
{% link 糖果屋教程贴, https://akilar.top/posts/615e2dec/, /img/android-chrome-192x192.png %}

<table width="80%" rules=none frame=void >
<tr>
<td style="border: none;">{% link 糖, https://akilar.top/posts/615e2dec/, /img/android-chrome-192x192.png %}</td>
<td style="border: none;">{% link 糖果屋, https://akilar.top/posts/615e2dec/, /img/android-chrome-192x192.png %}</td>
<td style="border: none;">{% link 糖果屋教程贴, https://akilar.top/posts/615e2dec/, /img/android-chrome-192x192.png %}</td>
</tr>
<tr>
<td style="border: none;">{% link 糖果, https://akilar.top/posts/615e2dec/, /img/android-chrome-192x192.png %}</td>
<td style="border: none;">{% link 糖果屋, https://akilar.top/posts/615e2dec/, /img/android-chrome-192x192.png %}</td>
<td style="border: none;">{% link 糖果屋教, https://akilar.top/posts/615e2dec/, /img/android-chrome-192x192.png %}</td>
</tr>
</table>

{% link 糖果, https://akilar.top/posts/615e2dec/, /img/android-chrome-192x192.png %}
{% link 糖果屋, https://akilar.top/posts/615e2dec/, /img/android-chrome-192x192.png %}
{% link 糖果屋教, https://akilar.top/posts/615e2dec/, /img/android-chrome-192x192.png %}
{% link 糖果屋教程, https://akilar.top/posts/615e2dec/, /img/android-chrome-192x192.png %}


| {% ghcard xaoxuu %} | {% ghcard xaoxuu, theme=vue %} |
| -- | -- |
| {% ghcard xaoxuu, theme=buefy %} | {% ghcard xaoxuu, theme=solarized-light %} |
| {% ghcard xaoxuu, theme=onedark %} | {% ghcard xaoxuu, theme=solarized-dark %} |
| {% ghcard xaoxuu, theme=algolia %} | {% ghcard xaoxuu, theme=calm %} |


| {% ghcard volantis-x/hexo-theme-volantis %} | {% ghcard volantis-x/hexo-theme-volantis, theme=vue %} |
| -- | -- |
| {% ghcard volantis-x/hexo-theme-volantis, theme=buefy %} | {% ghcard volantis-x/hexo-theme-volantis, theme=solarized-light %} |
| {% ghcard volantis-x/hexo-theme-volantis, theme=onedark %} | {% ghcard volantis-x/hexo-theme-volantis, theme=solarized-dark %} |
| {% ghcard volantis-x/hexo-theme-volantis, theme=algolia %} | {% ghcard volantis-x/hexo-theme-volantis, theme=calm %} |
