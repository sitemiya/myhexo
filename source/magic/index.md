comments: false
title: magic
---

## 写在前面

### 水纳百川终成海

这里记录了 Miya 日常工作中遇到的问题及其解决方案，也包含各种效率网站、生产力工具、编码及配置技巧。
思来想去，还是觉得以《实用工具》命名页面最为贴切。
创建这个页面的初衷是有太多散碎技能点需要整理。更倾向于在文章中记录系统的、或硬核的内容，因此，为这些散碎技能单独写作文章未免多余。目前的想法是按工具和问题的类型分组记录，这个页面就此诞生。

---

## 生产力

### 异地组网

<table width="80%" rules=none frame=void >
<tr>
<td style="border: none;">{% link SakuraFRP - 内网穿透, https://blog.miyamura.top/posts/magic-00.html/#SakuraFRP/, /img/android-chrome-192x192.png %}</td>
<td style="border: none;">{% link Zerotier - 虚拟局域网, https://blog.miyamura.top/posts/magic-00.html/#Zerotier/, /img/android-chrome-192x192.png %}</td>
<td style="border: none;">{% link Tailscale - 更好的 VPN？, https://blog.miyamura.top/posts/magic-00.html/#Tailscale/, /img/android-chrome-192x192.png %}</td>
</tr>
</table>
