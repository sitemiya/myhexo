# Miya's Hexo-GitlabPages project

[![pipeline status](https://gitlab.com/sitemiya/myhexo/badges/master/pipeline.svg)](https://gitlab.com/sitemiya/myhexo/-/commits/master)
[![Latest Release](https://gitlab.com/sitemiya/myhexo/-/badges/release.svg)](https://gitlab.com/sitemiya/myhexo/-/releases)


博客地址移步 [https://blog.miyamura.top](https://blog.miyamura.top)

## 更改日志

2023/11/20 - 2023/11/26 

本周进行博客的装修，包括页面设计、插件安装、css/js 引入和源文件魔改等

2023/11/28

尝试在 gitlab pages 部署

2024/01/08

更换域名，在新仓库部署。清除了图片缓存。

2024/01/24

细节优化，包括顶图波浪、右下角悬浮工具微调等

2024/01/25

细节优化，包括按钮颜色调整等

2024/03/05

页脚 badges 更换为国内链接，解决部分时段 403 导致标签无法加载的问题。

新增流水线条件，通过 commit message 中包含约定字符串的方式触发作业。

## 插件列表

### dependencies

| Plugin | Description |
| ------ | ------ |
| hexo-abbrlink  | 文章链接优化，利于 SEO |
| hexo-bilibili-bangumi  | 追番页，暂时用 demo 中的 uid 加载了番剧列表 |
| hexo-butterfly-categories-card  | 分类卡，暂时与网站不太协调，补课前端后会改 css 看看 |
| hexo-butterfly-clock-anzhiyu  | 侧边栏时钟 |
| hexo-butterfly-envelope  | 薇尔莉特的信箱 |
| hexo-butterfly-footer-beautify  | 页脚优化[@akilar](https://akilar.top/) |
| hexo-butterfly-footer-marcus  | 页脚优化[@heo](https://www.marcus233.top/) |
| hexo-butterfly-swiper-anzhiyu  | 人潮汹涌动画轮播图 |
| hexo-butterfly-wowjs  | 卡片动效 |
| hexo-generator-feed  | rss 订阅 |
| hexo-generator-indexed  | 主页文章列表依赖，同时提供首页文章隐藏 |
| hexo-generator-search  | 站内搜索 |
| hexo-generator-sitemap  | 搜索引擎 SEO 优化 |
| hexo-helper-live2d  | 页脚白猫 Live2d |
| hexo-renderer-ejs  | ? |
| hexo-renderer-marked  | ? |
| hexo-renderer-stylus  | stylus 模板引擎(?) |
| hexo-wordcount  | 字数统计依赖 |

### devDependencies

| Plugin | Description |
| ------ | ------ |
| hexo-generator-baidu-sitemap  | 百度 SEO 优化，可能仍需要手动提交站长平台 |

## 源文件修改

有点多。慢慢统计

## 待办

- [x] 备案
- [x] 配置图床
- [ ] 图床负载均衡
- [ ] CDN
- [ ] 说说的实现方案

- 目前的打算是，如果想写说说，就采用累积更新的策略写在 Markdown 文件中，并作为 hexo page 发布。

- [ ] 主题调整
- [ ] vercel 镜像

---

Forked from @VeraKolotyuk

[ci]: https://about.gitlab.com/gitlab-ci/
[hexo]: https://hexo.io
[install]: https://hexo.io/docs/index.html#Installation
[documentation]: https://hexo.io/docs/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[hexo-server]: https://hexo.io/docs/server.html
