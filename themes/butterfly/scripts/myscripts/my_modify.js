/* 'use strict';

const cheerio = require('cheerio');

hexo.extend.filter.register('after_render:html', function (str, data) {
    // 检查是否是导航页
    // if (data.path === 'path/to/generated/nav/index.html') {
	if (1) {
        const $ = cheerio.load(str, { decodeEntities: false });

        // 进行修改
        $('nav#nav').replaceWith(`
            <nav#nav>
                <span#blog_name>
                    <a#site-name(href="${hexo.config.url}/")">${hexo.config.title}</a>
                </span#blog_name>
                <div#menus>
                    ${partial('includes/header/menu_item', {}, {cache: true})}
                    <div#nav-right>
                        ${(theme.algolia_search.enable || theme.local_search.enable) ? '<div#search-button><a.site-page.social-icon.search><i.fas.fa-search.fa-fw></i></a></div>' : ''}
                        <div#toggle-menu><a.site-page><i.fas.fa-bars.fa-fw></i></a></div>
                    </div#nav-right>
                </div#menus>
            </nav#nav>
        `);

        // 返回修改后的 HTML
        return $.html();
    }

    // 对于其他页面，保持原样
    // return str;
}); */

/*
'use strict';
const { filter, extend } = hexo.extend;
const cheerio = require('cheerio');

// 添加 url_for 函数
extend.helper.register('url_for', hexo.extend.helper.get('url_for'));


function modifyNavStructure($) {
    const nav = $('#nav');
    if (nav.length === 0) return;

    // 移除原有结构
    nav.empty();

    // 添加新结构
    nav.append(`
        <span id="blog_name">
            <a id="site-name" href="${hexo.extend.helper.get('url_for')('/')}">${config.title}</a>
        </span>
        <div id="menus">
            ${partial('includes/header/menu_item', {}, { cache: true })}
        </div>
        <div id="nav-right">
            ${theme.algolia_search.enable || theme.local_search.enable
                ? '<div id="search-button"><a class="site-page social-icon search"><i class="fas fa-search fa-fw"></i></a></div>'
                : ''}
            <div id="toggle-menu">
                <a class="site-page"><i class="fas fa-bars fa-fw"></i></a>
                <div id="toggle-menu">
                    <a class="site-page"><i class="fas fa-bars fa-fw"></i></a>
                </div>
            </div>
        </div>
    `);
}

// 修改 HTML
filter.register('after_render:html', (str, data) => {
    const $ = cheerio.load(str, {
        decodeEntities: false
    });
    modifyNavStructure($);
    return $.html();
});
*/
/*
hexo.extend.filter.register('theme_inject', function(injectData) {
  const { config, theme, url_for, _p, partial } = hexo;

  // Check if the page being rendered is the navigation page
  const navPugPath = 'layout/include/header/nav.pug';
  if (injectData.view.path === navPugPath) {
    // Modify the structure of the navigation
    injectData.partial = `nav#nav
  span#blog_name
    a#site-name(href=${url_for('/')}) #[=${config.title}]

  #menus
  !=${partial('includes/header/menu_item', {}, { cache: true })}
  #nav-right
    if (theme.algolia_search.enable || theme.local_search.enable)
      #search-button
         a.site-page.social-icon.search
           i.fas.fa-search.fa-fw
    #toggle-menu
      a.site-page
        i.fas.fa-bars.fa-fw
      #toggle-menu
        a.site-page
          i.fas.fa-bars.fa-fw
    `;
  }
});
*/
