'use strict';
const { filter } = hexo.extend;
const cheerio = require('cheerio');

/**
 * 在页面插入新顶部图
 * @param {cheerio.Root} $ Root
 */
function insertTopImg($) {
    const header = $('#page-header');
    if (header.length === 0) return;
    const background = header.css('background-image');
    if (!background) return;
    // $('#post, #page, #archive, #tag, #category').prepend(`<div class="top-img" style="background-image: ${background};"></div>`);
	$('#post').prepend(`<div class="top-img" style="background-image: ${background};"></div>`);
}


/**
 * 替换指定元素的内容
 * @param {cheerio.Root} $ Root
 * @param {string} elementId 要替换的元素的ID
 * @param {string} newContent 替换后的内容（作为字符串）
 */
function changeElement($, elementId, newContent) {
    const element = $(`#${elementId}`);
    if (element.length === 0) return;

    element.html(newContent);
}

/**
 * 替换指定元素的内容
 * @param {cheerio.Root} $ Root
 * @param {string} elementClass 要替换的元素的CLASS
 * @param {string} newContent 替换后的内容（作为字符串）
 */
function changeElementByClass($, elementClass, newContent) {
    const element = $(`.${elementClass}`);
    if (element.length === 0) return;

    element.replaceWith(newContent);
}

// 修改 HTML
filter.register('after_render:html', (str, data) => {
    const $ = cheerio.load(str, {
        decodeEntities: false
    });
    insertTopImg($);
	
    const newContent_ = '';
    changeElementByClass($, 'avatar-img', newContent_);
	
	// 这段代码不生效，因为 footer-beauty 也是注入的
	// const newContent_ = '<p>haha</p>';
	// changeElement($, 'footer-bottom', newContent_);
	
    return $.html();
});
